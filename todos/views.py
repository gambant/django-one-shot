from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def todo_list_list(request):
    list = TodoList.objects.all()
    context = {
        "todo_list_list": list,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todos_object = TodoList.objects.get(id=id)
    context = {
        "todos_object": todos_object,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)

    else:
        form = TodoListForm()

    context = {"form": form}

    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            list = form.save()
        return redirect("todo_list_detail", id=list.id)

    # To add something to the model, like setting a user,
    # use something like this:
    #
    # model_instance = form.save(commit=False)
    # model_instance.user = request.user
    # model_instance.save()
    # return redirect("detail_url", id=model_instance.id)
    else:
        form = TodoListForm(instance=list)

    context = {"form": form}

    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)

    else:
        form = TodoItemForm()

    context = {"form": form}

    return render(request, "todos/items/create.html", context)


def todo_item_update(request, id):
    item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            item = form.save()
        return redirect("todo_list_detail", id=item.list.id)

    # To add something to the model, like setting a user,
    # use something like this:
    #
    # model_instance = form.save(commit=False)
    # model_instance.user = request.user
    # model_instance.save()
    # return redirect("detail_url", id=model_instance.id)
    else:
        form = TodoItemForm(instance=item)

    context = {"form": form}

    return render(request, "todos/items/edit.html", context)
